import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import aiohttp
import asyncio


async def fetch(n: complex, i):

    param = str(n.real) + "j" + str(n.imag) + ":" + str(i)
    url = "http://localhost:8080/" + param
    async with aiohttp.ClientSession() as session, session.post(url) as response:
        resp = await response.text()
        in_, it = resp.split(":")
        return int(it)


async def main():
    i = 10000
    w = 200
    matrix = np.zeros((w, w))

    sem = asyncio.Semaphore(1000)  # concurrent requests

    async def a(x, y):
        async with sem:
            matrix[x][y] = await fetch((x-(w/2))/(w/4) + (1j * (y-(w/2))/(w/4)), i)

    futures = []
    for x in range(w):
        for y in range(w):
            futures.append(a(x, y))

    await asyncio.gather(*futures)

    matrix = np.where(matrix == i, -1, matrix)
    m = np.amax(matrix)
    matrix = np.where(matrix == -1, m*1.1, matrix)

    plt.imshow(matrix, cmap="turbo_r")
    plt.show()


asyncio.run(main())


def test(a, b):
    matrix = np.zeros((a, b))
    matrix[1][1] = 200
    for x in range(a):
        for y in range(b):
            matrix[x][y] = x + y
    plt.imshow(matrix, cmap="plasma")


# matrix[1][1] = 200
# test(100,100)
