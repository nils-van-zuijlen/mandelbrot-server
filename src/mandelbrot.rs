use num_complex::{Complex, Complex64};

/// Is the complex `num` in the mandelbrot set?
///
/// - `num`: The complex number to test.
/// - `max_iterations`: The maximum number of iterations done in the mandelbrot suite
///
/// Returns a bool and an int:
/// - true if the number is in the set
/// - how many iterations was used to get that result
pub fn in_set(num: Complex<f64>, max_iterations: u32) -> (bool, u32) {
    let mut z = Complex64::new(0., 0.);
    let mut nb_iterations: u32 = 0;

    while nb_iterations != max_iterations && !(z.norm() > 2.) {
        z = (z*z) + num;
        nb_iterations += 1;
    }

    (z.norm() <= 2., nb_iterations)
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_tests() {
        assert_eq!(in_set(Complex::new(0., 0.), 100000), (true, 100000));
        assert_eq!(in_set(Complex::new(1., 1.), 10), (false, 2))
    }
}
