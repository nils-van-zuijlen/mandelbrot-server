extern crate num_complex;
extern crate web_server;

mod mandelbrot;

use std::str::FromStr;
use std::rc::Rc;
use std::collections::HashMap;
use std::thread;
use std::sync::{mpsc, Mutex, Arc};
use num_complex::Complex;

fn main() {

    let (tx, rx) = mpsc::channel();
    let receiving = Arc::new(Mutex::new(rx));
    let mut handles = vec![];

    for _ in 0..16 {
        let receiving = Arc::clone(&receiving);
        let handle = thread::spawn(move || {
            loop{
                let req = {
                    let rx = receiving.lock().unwrap();
                    rx.recv().unwrap()
                };
                process(req);
            }
        });
        handles.push(handle);
    }

    web_server::new()
        .get("/", Rc::new(|request, _|
            form(request)))
        .post("/:in", Rc::new(move |request, _| tx.send(request).unwrap()))
        .launch(8080);
}

fn form(req: web_server::Request) {
    req.send_resp("<form method=post><input name=in><input type=submit></form>".into());
}

fn process(req: web_server::Request) {
    let params = &req.params;
    let resp = process_synchronous(params);
    req.send_resp(resp);
}

/// Process a request for computations
///
/// Request should be a single value, in format 1.2j3.4:1000
/// with 1.2 the real part, 3.4 the imaginary part and 1000 the maximum number of iterations
///
/// The max_iterations can be omitted, default is 1000
fn process_synchronous(params: &HashMap<String, String>) -> web_server::Response {
    println!("params: {:?}", params);

    let param = match params.get("in") {
        Some(param) => param,
        None => return "Invalid input".into()
    };

    let p: &str = param;

    let (cplx, max_iterations) = match param.split_once(":") {
        Some((cplx, max_iterations)) => (cplx, max_iterations),
        None => (p, "1000")
    };

    let (real, imaginary) = match cplx.split_once('j') {
        Some((real, imaginary)) => (real, imaginary),
        None => return "Invalid complex number".into()
    };

    let real = f64::from_str(real).unwrap_or(3.);
    let imaginary = f64::from_str(imaginary).unwrap_or(3.);
    let max_iterations = u32::from_str(max_iterations).unwrap_or(1000);

    let num = Complex::new(real, imaginary);

    println!("cplx: {}, iter: {}", num, max_iterations);

    if num.norm() > 2. {
        return "false:0".into()
    };

    let (res, iterations) = mandelbrot::in_set(num, max_iterations);

    format!("{}:{}", res, iterations).into()
}
