import asyncio
import aiohttp

SERVER_URL = "http://localhost:8080/"


async def get_coords(n: complex, session: aiohttp.ClientSession, iterations: int = 10000) -> tuple[bool, int]:
    param = f"{n.real}j{n.imag}%3A{iterations}"
    print(repr(param))
    url = SERVER_URL + param
    async with session.post(url) as resp:
        text = await resp.text()
        print(text)
        in_set, iterations = text.split(":")

    return in_set == "true", int(iterations)


async def main():
    async with aiohttp.ClientSession() as session:
        res = await get_coords(0, session)
        print(res)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
