use crate::{HttpCode, RequestHandler, Request, Response};
use std::rc::Rc;

pub fn redirect(path: &'static str) -> RequestHandler {
    Rc::new(move |req: Request, mut res: Response| {
        res.set_http_code(HttpCode::_308);
        res.set_header("Location", path);

        req.send_resp(res);
    })
}
